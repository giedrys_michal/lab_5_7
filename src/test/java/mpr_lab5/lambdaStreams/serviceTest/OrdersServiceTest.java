package mpr_lab5.lambdaStreams.serviceTest;

import static org.assertj.core.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mpr_lab5.lambdaStreams.model.ClientDetails;
import mpr_lab5.lambdaStreams.model.Order;
import mpr_lab5.lambdaStreams.model.OrderItem;
import mpr_lab5.lambdaStreams.service.OrdersService;

public class OrdersServiceTest {
	Order order1;
	Order order2;
	Order order3;
	
	ClientDetails client1;
	ClientDetails client2;
	ClientDetails client3;
	ClientDetails client4;
	
	OrderItem item1;
	OrderItem item2;
	OrderItem item3;
	OrderItem item4;
	OrderItem item5;
	OrderItem item6;
	OrderItem item7;
	OrderItem item8;
	
	List<OrderItem> itemsList1;
	List<OrderItem> itemsList2;
	List<OrderItem> itemsList3;
	
	List<Order> orders;
	
	private final ByteArrayOutputStream output = new ByteArrayOutputStream();
	
	@Before
	public void prepareObjects(){
		client1 = new ClientDetails("jakislogin1", "Tytus", "Maliniak", 12);
		client2 = new ClientDetails("jakislogin_2", "Romek", "Nowak", 25);
		client3 = new ClientDetails("jakislogin_3", "Atomek", "Kołolsky", 17);
		client4 = new ClientDetails("inny_login_4", "Stefan", "Cebulak", 50);
		
		item1 = new OrderItem(1, "heheheherbata");
		item2 = new OrderItem(2, "japko");
		item3 = new OrderItem(3, "ciastki");
		item4 = new OrderItem(4, "mjut");
		item5 = new OrderItem(5, "gofery");
		item6 = new OrderItem(6, "dżemor");
		item7 = new OrderItem(7, "tfarug");
		
		itemsList1= Arrays.asList(item1, item2, item3);
		itemsList2= Arrays.asList(item2, item3, item4, item5, item6, item7);
		itemsList3= Arrays.asList(item1, item2, item3, item4, item5, item6);
		
		order1 = new Order(client1, itemsList1, "aaa komentarz zaczyna się na a");
		order2 = new Order(client2, itemsList2, "");
		order3 = new Order(client3, itemsList3, "inny koment");
		
		orders = Arrays.asList(order1, order2, order3);
		
		System.setOut(new PrintStream(output));
	}
	
	// #1
	@Test
	public void FindOrdersWhichHaveMoreThan5OrderItems() {
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		assertThat(result).containsOnly(order2);
	}
	
	// #2
	@Test
	public void FindOldestClientAmongThoseWhoMadeOrders() {
		ClientDetails result = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		client4.equals(assertThat(result));
	}
	
	// #3
	@Test
	public void FindOrderWithLongestComments() {
		Order result = OrdersService.findOrderWithLongestComments(orders);
		order1.equals(assertThat(result));
	}
	
	// #4
	@Test
	public void GetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		assertThat(result).isEqualTo("Romek Nowak, Stefan Cebulak");
	}
	
	// #5
	@Test
	public void GetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		assertThat(result).isEqualTo(Arrays.asList("ciastki", "heheheherbata", "japko"));
	}
	
	// #6
	@Test
	public void PrintCapitalizedClientsLoginsWhoHasNameStartingWithS() {
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
		assertThat("INNY_LOGIN_4").isEqualTo(output.toString());
	}
	
	// #7
	@Test
	public void GroupOrdersByClient() {
		Map<ClientDetails, List<Order>> expectedResult = new HashMap<ClientDetails, List<Order>>(); 
		expectedResult.put(client1, Arrays.asList(order1));	
		expectedResult.put(client2, Arrays.asList(order2));
		expectedResult.put(client3, Arrays.asList(order3));			
		
		Map<ClientDetails, List<Order>> actualResult = OrdersService.groupOrdersByClient(orders);
		
		assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	// #8
	@Test
	public void PartitionClientsByUnderAndOver18() {
		Map<Boolean, List<ClientDetails>> expectedResult = new HashMap<>();
		expectedResult.put(false, Arrays.asList(client1, client3));
		expectedResult.put(true, Arrays.asList(client2, client4));
		
		Map<Boolean, List<ClientDetails>> actualResult = OrdersService.partitionClientsByUnderAndOver18(orders);
		
		assertThat(actualResult).isEqualTo(expectedResult);
	}
	
	@After
	public void clean() {
		System.setOut(null);
	}
}
