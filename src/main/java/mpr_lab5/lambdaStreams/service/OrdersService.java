package mpr_lab5.lambdaStreams.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import mpr_lab5.lambdaStreams.model.ClientDetails;
import mpr_lab5.lambdaStreams.model.Order;

public class OrdersService {

    // #1
	public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
    	orders.stream()
    			.filter(order -> order.getItems().size() > 5);    	
    	
    	return orders;
    }

	// #2
	public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	ClientDetails oldestClient = orders.stream()
    			.map(order -> order.getClientDetails())
    			.max(Comparator.comparing(ClientDetails::getAge))
    			.get();
    	
    	return oldestClient;
    }

	// #3
	public static Order findOrderWithLongestComments(List<Order> orders) {
    	Order orderWithLongestComment = orders.stream()
    			.max(Comparator.comparing(Order::getComments))
    			.get();
    	
		return orderWithLongestComment;
    }

	// #4
	public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	String commaString = orders.stream()
    			.filter(order -> order.getClientDetails().getAge() > 18)
    			.map(order -> order.getClientDetails().getName() + " " + order.getClientDetails().getSurname())
    			.collect(Collectors.joining(", "));
    	
    	return commaString;

    }

	// #5
	public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
    	List<String> orderNames = orders.stream()
    			.filter(order -> order.getComments().startsWith("A") || order.getComments().startsWith("a"))
    			.map(order -> order.getItems())
    			.flatMap(list -> list.stream())
    			.map(item -> item.getName())
    			.distinct()
    			.sorted()
    			.collect(Collectors.toList());
    	
    	return orderNames;
    }

	// #6
	public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
    	orders.stream()
    			.map(order -> order.getClientDetails())
    			.filter(client -> client.getLogin().toUpperCase().matches("^S.*"))
    			.forEach(System.out::println);
    }

	// #7
	public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	Map<ClientDetails, List<Order>> groupedByClient = orders.stream()
    			.collect(Collectors.groupingBy(Order::getClientDetails));
    	
    	return groupedByClient;
    }

	// #8
	public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        Map<Boolean, List<ClientDetails>> partition = orders.stream()
        		.map(order -> order.getClientDetails())
        		.collect(Collectors.partitioningBy(client -> client.getAge() > 18));
        
        return partition;
    }

}
